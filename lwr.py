#!/usr/bin/env python
###################################################################################
# Tool to generate (LAVA) weekly reports
# Copyright (C) 2016 Luis Araujo <luis.araujo@collabora.co.uk>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

from argparse import ArgumentParser

from lqa_tool.settings import settings
from lqa_api.connection import Connection
from lqa_api.job import Job
from lqa_api.utils import job_id_list_from_range

LAVA_LINK="https://lava.collabora.co.uk/scheduler/job/"

__version__ = 0.1

settings.load_config()
conn = Connection(settings.rpc_url)

class Cli(object):
    """Command line interface using argparse"""

    def __init__(self):
        self.parser = ArgumentParser(description="lwr v{}".format(__version__))
        # Sub-commands
        subparsers = self.parser.add_subparsers()

        # Fetch results
        fetch_parser = subparsers.add_parser('fetch', help="Fetch results")
        fetch_parser.add_argument('job_ids', type=str, nargs='+',
                                  metavar='JOB_ID_OR_RANGE',
                                  help="Job id or jobs ID's range")
        fetch_parser.add_argument('platform', type=str, metavar='IMAGETYPE-ARCH',
                                  help="Platform name with format: "
                                  "<image-type>-<arch>")
        fetch_parser.set_defaults(func=fetch)

        # Generate wiki report (Wikify)
        wikify_parser = subparsers.add_parser('wikify', help="Wikify results")
        wikify_parser.add_argument('result_file', type=str, nargs='+',
                                   metavar='IMAGETYPE-ARCH.txt',
                                   help="The imagetype-arch.txt files generated "
                                   "by the 'fetch' command")
        wikify_parser.add_argument('--tester', type=str, default='NONAME',
                                   help="Tester name")
        wikify_parser.set_defaults(func=wikify)

    def run(self):
        args = self.parser.parse_args()
        try:
            args.func(args)
        except KeyboardInterrupt:
            pass

def wikify(args):

    def _print_wiki(test, arch, found_lava_links):
        """This function prints the wiki"""
        if result_table[test].get(arch, False):
            result_value = result_table[test][arch][0]
            print '| bgcolor={{#var:' + result_value + '}} | ' + result_value
            found_lava_links.append(result_table[test][arch][1])
        else:
            print '| bgcolor=|'

    files_lines = []
    for rfile in args.result_file:
        with open(rfile) as fp:
            files_lines.extend(fp.readlines())

    result_table = {}
    for line in files_lines:
        (test, result, lavalink, arch) = line.split(' ')
        arch = arch.strip()

        if not result_table.get(test, False):
            result_table[test] = {}

        result_table[test][arch] = (result, lavalink)
        
    for test in result_table.keys():
        found_lava_links = []
        print "+++ {}".format(test)
        # Get and sort out the platform order from the result files passed from
        # the command line.
        for platform in args.result_file:
            _print_wiki(test, platform.split('.')[0], found_lava_links)
        print '| {}'.format(args.tester)
        print '| ' + " ".join(['[' + l + ']' for l in found_lava_links])
        print

def fetch(args):
    list_of_jobs = job_id_list_from_range(args.job_ids)
    result_table = {}
    final_lines = []

    # Collect and sort results.
    for job_id in list_of_jobs:
        job = Job(job_id, conn)
        for result_line in job.results:
            line = result_line.strip()
            (jobid, test, testid, tcid, result) = line.split(':')
            if not result_table.get(testid, False):
                result_table[testid] = {}
                result_table[testid]['result'] = {}
                result_table[testid]['result']['pass'] = 0
                result_table[testid]['result']['fail'] = 0
                result_table[testid]['result']['other'] = 0
                result_table[testid]['jobid'] = jobid

            if result == 'pass':
                result_table[testid]['result']['pass'] =+ 1
            elif result == 'fail':
                result_table[testid]['result']['fail'] =+ 1
            else:
                result_table[testid]['result']['other'] =+ 1

    # Set proper status for each result.
    for testid in result_table.keys():
        if result_table[testid]['result']['pass'] > 0:
            if result_table[testid]['result']['fail'] > 0 or \
               result_table[testid]['result']['other'] > 0:
                status = "PARTIAL"
            else:
                status = "PASSED"
        elif result_table[testid]['result']['fail'] > 0:
            status = "FAILED"
        else:
            status = "UNKNOWN"
        final_lines.append("{} {} {} {}".format(testid, status, \
                        LAVA_LINK+result_table[testid]['jobid'], args.platform))

    # Write the final result in file <platform>.txt
    with open("{}.txt".format(args.platform), 'w') as fp:
        fp.write("\n".join(final_lines))
        
lwr = Cli()
lwr.run()
