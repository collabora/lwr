LAVA Weekly Report Tool (lwr)

This tool is used to fetch the LAVA automated tests results and generate the weekly
report in wiki format.

Find the latest code from the git repository:

	https://git.collabora.com/cgit/user/araujo/lwr.git/

Install:

`lwr` uses the `lqa` API, so `lqa` needs to be installed first:

	https://git.collabora.com/cgit/singularity/tools/lqa.git/

Usage:

Just execute directly from the command line:

```
$ ./lwr	-h
```

It has the following options:

```usage: lwr.py [-h] {fetch,wikify} ...

lwr v0.1

positional arguments:
  {fetch,wikify}
    fetch         Fetch results
    wikify        Wikify results

optional arguments:
  -h, --help      show this help message and exit

```

The workflow is as follow:

- First, the results need to be fetched for a specific image type using the `fetch` subcommand.
This operation needs to be executed for every image type we want to collect the results from, and
it requires the ID for the LAVA jobs.

Note: One easy way to fetch the LAVA jobs ID's is using the LAVA webUI `All Jobs` page:
https://lava.collabora.co.uk/scheduler/alljobs , where the full name and ID of the job is clearly
available. The Search box can be used to filter the list of jobs for a specific image type/version.

For example to fetch all the tests results for the SDK i386 and Target AMD 64 images for a given version:

```
$ ./lwr.py fetch 289007 292063 289009 289010 289011 sdk-i386
$ ./lwr.py fetch 293289 291683 291684 293290 target-amd64
```

These commands will generate a file for each platform containing the automated tests results and with name 
`<imagetype>-<arch>.txt`, in the above example, the files will be `sdk-i386.txt` and `target-amd64.txt`.

- The second step is to generate the report in wiki format using the results files created previously by
the `fetch` subcommand. This step only needs to be executed **once**, but it needs to include the result
files for each platform of which we want to include the results in the report. These files also need to be 
ordered in the way we want the results to appear in the report, for example, if we want to get the report with
the target amd64 results in the first column and sdk i386 results in the second column:

```
$ ./lwr.py wikify --tester Araujo target-amd64.txt sdk-i386.txt > wiki-report.txt
```

This command will generate the wiki report (which goes to standard output by default) and saves it in the
file `wiki-report.txt` with the target amd64 results in the first column and the sdk i386 results in the 
second column. It also sets the name of the tester to Araujo (otherwise it will set it to the default `NONAME`
value if the `--tester` option is not passed).

Luis Araujo
luis.araujo@collabora.co.uk
